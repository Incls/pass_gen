# pass_gen

Simple password generator.

Usage:

python3.x pass_gen.py

For better availability, I recommend that to .bashrc:
alias pypass="python3.6 /root/.run/pass_gen.py"

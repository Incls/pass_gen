#!/usr/bin/python3
import random
import string
import re
import codecs

def hexa(x):
    num_hex = hex(x).split('x')[-1]
    return num_hex
def gen():
    strin = string.ascii_letters + string.digits + string.punctuation
    random_strin = random.randint(15, 20)
    passw = ""
    for i in range(0, int(random_strin)):
        passw += random.choice(strin)
    lista = [passw]
    for line in passw:
        for c in line:
            lista.append(c)
    lista.pop(0)
    how_pass = len(re.sub('[^0-9]', '', passw))
    numbers = re.findall('\d+', passw)
    if numbers == []:
        numbers += '1'
    numbers = list(map(int, numbers))
    numbers = (''.join(map(str, numbers)))
    num_eff = hexa(int(numbers))
    eff = num_eff
    end = ''.join('%s%s' % (x, random.choice(num_eff) if random.random() > 0.5 else '') for x in passw) #random poss

    print("Password: " + end)
gen()
